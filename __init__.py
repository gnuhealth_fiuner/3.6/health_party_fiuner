from trytond.pool import Pool
from .health_party import *
from .address import *

def register():
    Pool.register(
        Party,
        Address,
        Partido,
        module='health_party_fiuner', type_='model')
