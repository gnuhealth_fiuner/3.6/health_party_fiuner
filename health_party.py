#-*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from trytond import backend

__all__ = ['Party']

class Party(metaclass = PoolMeta):    
    __name__ = 'party.party'

    @classmethod
    def __register__(cls, module_name):        
        super(Party, cls).__register__(module_name)
        # Migration from 2.8 'dni' column to 'ref' column
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        sql_table = cls.__table__()
        cursor = Transaction().connection.cursor()
        
        if table.column_exist('dni'):            
            cursor.execute(*sql_table.update(
                    columns=[sql_table.ref],
                    values=[sql_table.dni],
                    where=sql_table.is_person=='True'
                    )) 
            table.drop_column('dni')
        
        if table.column_exist('nivel_educativo'):            
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=[None],
                    where=sql_table.nivel_educativo=='sin_dato'))            
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['1'],
                    where=sql_table.nivel_educativo=='primario_incompleto'))            
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['2'],
                    where=sql_table.nivel_educativo=='primaria_completa'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['attending_primary_school'],
                    where=sql_table.nivel_educativo=='cursando_primaria'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['3'],
                    where=sql_table.nivel_educativo=='secundaria_incompleta'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['4'],
                    where=sql_table.nivel_educativo=='secundaria_completa'))            
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['attending_secondary_school'],
                    where=sql_table.nivel_educativo=='cursando_secundaria'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['tertiary_school'],
                    where=sql_table.nivel_educativo=='terciario_completo'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['incomplete_tertiary_school'],
                    where=sql_table.nivel_educativo=='terciario_incompleto'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['attending_tertiary_school'],
                    where=sql_table.nivel_educativo=='cursando_terciario'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['5'],
                    where=sql_table.nivel_educativo=='universidad_completa'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['attending_university'],
                    where=sql_table.nivel_educativo=='cursando_universidad'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.nivel_educativo],
                    values=['universidad_incompleta'],
                    where=sql_table.nivel_educativo=='universidad_incompleta'))
            cursor.execute(*sql_table.update(
                    columns=[sql_table.education],
                    values=[sql_table.nivel_educativo],
                    where=sql_table.is_person=='True'
                    ))
            table.drop_column('nivel_educativo')
     
