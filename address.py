#-*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta


__all__ = ['Address', 'Partido']


class Address(metaclass = PoolMeta):    
    __name__ = 'party.address'
    
    partido = fields.Many2One('party.address.partido', 'Partido')
    
class Partido(ModelSQL,ModelView):
    "Partido"
    __name__ = 'party.address.partido'
    
    name = fields.Char('Nombre')
    
